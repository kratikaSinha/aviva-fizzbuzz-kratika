import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import NavBar from './components/AppHeader';
import NumberInput from './components/NumberInput';
import ResultTable from './components/ResultTable';
import Pagination from './components/Pagination';

class App extends React.Component {
  state = {
    startNumber: 0,
    totalNumber: 0,
    endNumber: 0,
    itemsPerPage: 20,
  };

  handleNumberChange = (value) => {
    if (value >= 0) {
      this.setState({
        totalNumber: value,
        startNumber: value > 0 ? 1 : 0,
        endNumber: value > this.state.itemsPerPage ? this.state.itemsPerPage : value,
      });
    }
  };

  updatePageNumber = (startNumber, endNumber) => {
    this.setState({
      startNumber,
      endNumber,
    });
  };

  render() {
    return (
      <div className="App" data-test="app-test">
        <NavBar title="FizzBuzz Application" />
        <div className="container text-center" data-test="app-input-test">
          <NumberInput value={this.state.totalNumber} handleNumberChange={this.handleNumberChange} />
          <ResultTable startNumber={this.state.startNumber} endNumber={this.state.endNumber} />
          <Pagination
            totalNumber={this.state.totalNumber}
            updatePageNumber={this.updatePageNumber}
            itemsPerPage={this.state.itemsPerPage}
          />
        </div>
      </div>
    );
  }
}

export default App;
