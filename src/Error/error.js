import React from "react";
import PropTypes from 'prop-types';

class Error extends React.Component {
  render() {
    return (this.props.error ? <h6 data-test='error-test'> {
      this.props.error
    } </h6> : <div data-test='no-error-test' />);
  }
}

Error.defaultProps = {
  error: "",
};

Error.propTypes = {
  error: PropTypes.string,
};

export default Error;