import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import Error from './error';

Enzyme.configure({ adapter: new EnzymeAdapter() });

it('renders without error when no error is given', () => {

  const wrapper = shallow(<Error />);
  const errComponent = wrapper.find("[data-test='no-error-test']");
  expect(errComponent.length).toBe(1);
});

it('renders without error when error is given', () => {

  const wrapper = shallow(<Error error="some error" />);
  const errComponent = wrapper.find("[data-test='error-test']");
  expect(errComponent.length).toBe(1);
});
