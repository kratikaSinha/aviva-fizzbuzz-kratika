import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import App from './App';

Enzyme.configure({ adapter: new EnzymeAdapter() });

it('renders without error', () => {

  const wrapper = shallow(<App />);
  const appComponent = wrapper.find("[data-test='app-test']");
  expect(appComponent.length).toBe(1);
});

it('Should contain main component', () => {

  const wrapper = shallow(<App />);
  const appComponent = wrapper.find("[data-test='app-input-test']");
  expect(appComponent.length).toBe(1);
});