import Enzyme from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

Enzyme.configure({
  adapter: new EnzymeAdapter(),
  disableLifecycleMethods: true
});

import { validateInputNumber, NEGATIVE_NUMBER, MAX_NUMBER } from './validation';

describe('Global Functions: validateInputNumber', () => {

  it('Should not accept negative value', () => {
    expect(validateInputNumber(-10)).toBe(NEGATIVE_NUMBER);
  });

  it('Should not accept number between 1 to 1000', () => {
    expect(validateInputNumber(0)).toBe(MAX_NUMBER);
  });

  it('Should return blank for number 100', () => {
    expect(validateInputNumber(100)).toBe('');
  });

});
