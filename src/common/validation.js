const MIN_NUM = 0;
const MAX_NUM = 1000;

export const NEGATIVE_NUMBER = "Please enter positive number";
export const MAX_NUMBER = `Please enter value between ${MIN_NUM + 1} to ${MAX_NUM}`;

export const validateInputNumber = num => {
  return num < MIN_NUM ?
    NEGATIVE_NUMBER :
    num > MAX_NUM || num <= MIN_NUM ? MAX_NUMBER : '';
};