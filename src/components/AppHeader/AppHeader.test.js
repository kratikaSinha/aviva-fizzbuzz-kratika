import React from "react";
import NavBar from "../AppHeader/index";
import { shallow } from "enzyme";

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<NavBar {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const findByTestAtr = (wrapper, dataTestValue) => {
  return wrapper.find(`[data-test="${dataTestValue}"]`);
};

it("renders a component display", () => {
  const wrapper = setup();
  const navbarDisplay = findByTestAtr(wrapper, "navbar-display");
  expect(navbarDisplay.length).toBe(1);
});