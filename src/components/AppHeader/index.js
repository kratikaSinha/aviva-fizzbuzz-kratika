import React from 'react';
import PropTypes from 'prop-types';

const styles = {
  fontWeight: 'normal'
};

const NavBar = props => {
  return (
    <nav className="navbar navbar-dark bg-primary" data-test="navbar-display">
      <h6 className="navbar-brand" style={styles}>
        {props.title}
      </h6>
    </nav>
  );
};

NavBar.defaultProps = {
  title: ""
};

NavBar.propTypes = {
  title: PropTypes.string
};

export default NavBar;