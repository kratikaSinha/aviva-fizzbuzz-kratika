import React from 'react';
import { shallow } from 'enzyme';
import NumberInput from './index';

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<NumberInput {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const findByTestAtr = (wrapper, dataTestValue) => wrapper.find(`[data-test="${dataTestValue}"]`);

it('renders a component display', () => {
  const wrapper = setup();
  const inputDisplay = findByTestAtr(wrapper, 'input-display');
  expect(inputDisplay.length).toBe(1);
});

it('should call onChange function', () => {
  const handleNumberChange = jest.fn();
  const event = {
    target: { value: 5 },
  };
  const component = shallow(<NumberInput handleNumberChange={handleNumberChange} />);
  component.find('input').simulate('change', event);
  expect(handleNumberChange).toBeCalledWith(event.target.value);
});