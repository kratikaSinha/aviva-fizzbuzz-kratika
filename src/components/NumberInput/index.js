import React from 'react';
import PropTypes from 'prop-types';
import { validateInputNumber } from '../../common/validation';
import Error from '../../Error/error';

class NumberInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      error: '',
    };
  }

  handleChange = (value) => {
    const invalid = validateInputNumber(Number(value));
    const { handleNumberChange } = this.props;
    this.setState({
      error: invalid,
    });
    if (invalid) handleNumberChange(0);
    else handleNumberChange(value);
  };

  render() {
    const { error } = this.state;
    return (< div data-test="input-display" >

      <h5 className="mt-2" > Please Enter Positive Number </h5>
      <input data-test="userInput"
        id="userInput"
        type="number"
        className="mb-5 mt-2"
        value={this.props.value > 0 ? this.props.value : ''}
        onChange={
          (e) => {
            this.handleChange(Number(e.target.value));
          }
        }
      />
      <Error error={error} />  </div>
    );
  }
}

NumberInput.defaultProps = {
  value: 0,
  handleNumberChange: () => { },
};

NumberInput.propTypes = {
  value: PropTypes.number,
  handleNumberChange: PropTypes.func,
};

export default NumberInput;