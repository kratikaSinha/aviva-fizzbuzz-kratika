import React from 'react';
import PropTypes from 'prop-types';

export const checkDay = date => date.getDay() === 3;

export const generateDataList = (start, end, date) => {
  const dataList = [];
  if (start !== 0 && end !== 0) {
    const getElem = (title, color) => <span style={{ color }}>{title}</span>;

    for (let i = start; i <= end; i += 1) {
      if (i % 5 === 0 && i % 3 === 0) {
        checkDay(date)
          ? dataList.push({
            id: i,
            data: (
              <div>
                {getElem('wizz', 'blue')} {getElem('wuzz', 'green')}
              </div>
            ),
          })
          : dataList.push({
            id: i,
            data: (
              <div>
                {getElem('fizz', 'blue')} {getElem('buzz', 'green')}
              </div>
            ),
          });
      } else if (i % 5 === 0) {
        checkDay(date)
          ? dataList.push({
            id: i,
            data: <div>{getElem('wuzz', 'green')} </div>,
          })
          : dataList.push({
            id: i,
            data: <div>{getElem('buzz', 'green')}</div>,
          });
      } else if (i % 3 === 0) {
        checkDay(date)
          ? dataList.push({
            id: i,
            data: <div>{getElem('wizz', 'blue')}</div>,
          })
          : dataList.push({
            id: i,
            data: <div>{getElem('fizz', 'blue')}</div>,
          });
      } else {
        dataList.push({
          id: i,
          data: <div>{getElem(i, 'black')}</div>,
        });
      }
    }
  }
  return dataList;
};

const ResultTable = (props) => {
  const { endNumber, startNumber } = props;
  return (
    <ul className="list-group" data-test="result-table">
      {generateDataList(startNumber, endNumber, new Date()).map(item => (
        <li className="list-group-item" key={item.id}>
          {item.data}
        </li>
      ))}
    </ul>
  );
};

ResultTable.defaultProps = {
  endNumber: 0,
  startNumber: 0,
};

ResultTable.propTypes = {
  endNumber: PropTypes.number,
  startNumber: PropTypes.number,
};

export default ResultTable;