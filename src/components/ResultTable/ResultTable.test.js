import React from 'react';

import { shallow } from 'enzyme';
import ResultTable, { checkDay } from './index';

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<ResultTable {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const findByTestAtr = (wrapper, dataTestValue) => wrapper.find(`[data-test="${dataTestValue}"]`);

it('checkDay return true for Wednesday and false for any other day', () => {
  let date = new Date('08-07-2019');
  expect(checkDay(date)).toBe(true);
  date = new Date('08-08-2019');
  expect(checkDay(date)).toBe(false);
});

it('renders a component display', () => {
  const wrapper = setup();
  const resultTable = findByTestAtr(wrapper, 'result-table');
  expect(resultTable.length).toBe(1);
});

it('should render result list given a startNumber and endNumber', () => {
  const startNumber = 1;
  const endNumber = 5;
  const resultTable = shallow(<ResultTable startNumber={startNumber} endNumber={endNumber} />);
  const numLis = resultTable.find('li').length;
  expect(numLis).toBe(endNumber - startNumber + 1);
});
