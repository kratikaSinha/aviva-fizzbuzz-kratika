import React from 'react';
import PropTypes from 'prop-types';

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startNumber: 0,
      endNumber: 0,
      currentPage: 1,
      totalNumber: this.props.totalNumber,
      itemsPerPage: this.props.itemsPerPage
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { totalNumber, itemsPerPage } = prevState;
    if (nextProps.totalNumber !== totalNumber) {
      return ({
        startNumber: nextProps.totalNumber > 0 ? 1 : 0,
        endNumber: nextProps.totalNumber > itemsPerPage ? itemsPerPage : nextProps.totalNumber,
        currentPage: 1,
        totalNumber: nextProps.totalNumber,
        itemsPerPage: nextProps.itemsPerPage
      });
    }
    return nextProps;
  }


    getRange = (pageNumber) => {
      const { itemsPerPage, totalNumber } = this.props;

      let startNumber = itemsPerPage * (pageNumber - 1) + 1;
      let endNumber = itemsPerPage * pageNumber;
      if (endNumber > totalNumber) {
        endNumber = totalNumber;
      }
      if (pageNumber <= 1) {
        startNumber = 1;
        endNumber = totalNumber <= itemsPerPage ? totalNumber : itemsPerPage;
      }
      return [startNumber, endNumber];
    };

    handlePageChange = (newPageNumber) => {
      const { itemsPerPage, totalNumber, updatePageNumber } = this.props;
      if (newPageNumber >= 1 && newPageNumber <= Math.floor(totalNumber / itemsPerPage - 0.001) + 1) {
        const range = this.getRange(newPageNumber);
        this.setState({
          startNumber: range[0],
          endNumber: range[1],
          currentPage: newPageNumber,
        });
        updatePageNumber(range[0], range[1]);
      }
    };

    render() {
      const { startNumber, endNumber, currentPage } = this.state;
      const { totalNumber } = this.props;
      return (
            <>
                {totalNumber > 20 && (
                  <div className="container text-right mt-2 mb-2" data-test="pagination-display">
                    <span className="mr-1">
                      {startNumber} - {endNumber} out of {totalNumber}
                    </span>
                    <button
                      type="button"
                      data-test="page-decrement"
                      onClick={() => {
                        this.handlePageChange(currentPage - 1);
                      }}
                      disabled={startNumber === 1}
                    >
                            Previous
                    </button>
                    <button
                      type="button"
                      data-test="page-increment"
                      onClick={() => {
                        this.handlePageChange(currentPage + 1);
                      }}
                      disabled={endNumber >= totalNumber}
                    >
                            Next
                    </button>
                  </div>
                )}
            </>
      );
    }
}

Pagination.defaultProps = {
  updatePageNumber: () => { },
  totalNumber: 0,
  itemsPerPage: 0,
};

Pagination.propTypes = {
  updatePageNumber: PropTypes.func,
  totalNumber: PropTypes.number,
  itemsPerPage: PropTypes.number,
};

export default Pagination;