import React from 'react';
import Pagination from '../Pagination/index';
import { shallow } from 'enzyme';

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<Pagination {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const findByTestAtr = (wrapper, dataTestValue) => {
  return wrapper.find(`[data-test="${dataTestValue}"]`);
};

it('renders a component display', () => {
  const updatePageNumberMock = jest.fn();
  let props = {
    itemsPerPage: 20,
    totalNumber: 56,
    updatePageNumber: updatePageNumberMock
  };
  const wrapper = setup(props);
  const pagination = findByTestAtr(wrapper, 'pagination-display');
  expect(pagination.length).toBe(1);
});

it('should call handlePage function', () => {
  const updatePageNumberMock = jest.fn();
  let props = {
    itemsPerPage: 20,
    totalNumber: 56,
    updatePageNumber: updatePageNumberMock
  };
  let state = {
    startNumber: 0,
    endNumber: 0,
    currentPage: 1
  };
  const wrapper = setup(props, state);

  const incrementButton = findByTestAtr(wrapper, 'page-increment');
  let currentPage = wrapper.state('currentPage');
  incrementButton.simulate('click');
  expect(wrapper.state('currentPage')).toBe(currentPage + 1);

  const decrementButton = findByTestAtr(wrapper, 'page-decrement');
  currentPage = wrapper.state('currentPage');
  decrementButton.simulate('click');
  expect(wrapper.state('currentPage')).toBe(currentPage - 1);
});