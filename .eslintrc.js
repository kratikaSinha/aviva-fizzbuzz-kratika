module.exports = {
    env: {
        browser: true,
        es6: true,
        jest: true
    },
    parser: "babel-eslint",
    extends: [
        'eslint:recommended',
        "plugin:react/recommended"
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        'react',
    ],
    rules: {
        "react/jsx-uses-react": "error",
        "react/jsx-uses-vars": "error",
        "consistent-return": 2,
        "indent": [1, 2],
        "no-else-return": 1,
        "semi": [1, "always"],
        "space-unary-ops": 2,
        "camelcase": "error",
        "space-infix-ops": ["error"],
        "no-console": "off",
        "no-debugger": "off"

    },
};